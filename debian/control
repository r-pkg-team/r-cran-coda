Source: r-cran-coda
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Chris Lawrence <lawrencc@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-coda
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-coda.git
Homepage: https://cran.r-project.org/package=coda
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-lattice
Testsuite: autopkgtest-pkg-r

Package: r-cran-coda
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Output analysis and diagnostics for MCMC simulations in R
 This package provides output analysis and diagnostics for Markov Chain
 Monte Carlo simulations and estimations within R.  It also includes some
 graphical analysis routines and a facility for importing output from BUGS
 (Bayes Using Gibbs Sampling), a popular tool for running MCMC simulations.
 .
 The related r-cran-mcmcpack package includes MCMC estimators for some
 common models in the social sciences.
